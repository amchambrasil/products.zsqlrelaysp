.. contents:: Table of Contents
   :depth: 2

*********************
Products.ZSQLRelaySP
*********************


Overview
--------

SQLRelayOracleSP allows to call stored procedures and functions of
Oracle database and allows these functions to return values (including OUT parameters)

Values may be returned in three ways:

* OUT parameters (or IN/OUT)
* single values returned by function like: select f_name from dual
* cursors returned by function

SQLRelayOracleSP uses methods described at SQLRelay documentation at:
"Python API":http://sqlrelay.sourceforge.net/sqlrelay/programming/python.html

In parameters for procedure object are:

Call template
---------------

May look like:
* select testproc(:in1,:in2,:in) from dual
* begin testproc(:in1,:in2,:in3,:out1,:out2,:out3); end;
* begin  :curs:=testproc; end;
* select * from tabela
* select imie, nazwisko from tabela

BEWARE! in case of parameters of type DATE it is necessary to use:

**begin testproc(:in1, to_date(:data, 'YYYY-MM-DD')); end;**

or similiar with proper date format as used by database.
Parameter that is passed to variable should be string


Definition of parameters (IN and OUT)
-------------------------------------

Parameters are written in template like ':name', eg.

**:in1 :in2**

When calling procedure object you need to pass values to it. For example
if you have function called ''testowa'' (zope id) with template like::

    ''begin testproc(:in1, :out3); end;''

then you may call it like::

    ''context.testowa(in1='tekst', out3=out)''
    ''context.testowa('tekst', out)''

All parameters from template of function/procedure have to be
mentioned in one (or two for IN/OUT parameters) groups of parameters.
These groups are IN and OUT.
Internally, according to assignment of parameters to
groups, ZSQLRelaySP uses different methods to bind IN/OUT parameters
(as described in SQLRelay docs).

For IN parameters it is necessary to enter parameter name, type and optionally
''precision'' or ''length'' and ''scale'' for specific field. By default
length and scale are 0.

''Precision'' and ''scale'' are used with numbers, lenght is used with strings.
''Precision'' is a count of all digits in number and ''scale'' is the number of
float digits (? english translation problem).
''Length'' is a number of characters in string.

With OUT parameters it is necessary to enter parameter name, type, precision or length
and optionally ''scale''

BEWARE! When value returned by procedure is longer than ''length'' definied in
definition then procedure will return None for ALL parameters!

Proper data types are: VARCHAR, CHAR, LONG, NUMBER, INTEGER, DATE,
BLOB, CLOB, DECIMAL, NUMERIC.
Unrecognized data types are converted to string.


Result set
----------

it is name of parameter that is bound to result_set returned by function.
Should be used only when function returns cursor, eg.::

    ''create or replace function em_cursor return types.cursortype is
        l_cursor    types.cursorType;
    begin
        open l_cursor for select * from employees;
        return l_cursor;
    end;''
