import DateTime
import logging
logger=logging.getLogger('coig_utils')

def get_ora_in_conv(description):
    """ Returns converted value (input to database)
        @description - dictionary with: 'name', 'type', 'sqltype', 'precision', 'scale'        
    """
    # conversion to NLS_NUMERIC_CHARACTERS='. '
    #import os
    #env_vars = os.environ
    #nr_chars = ', '
    #if 'NLS_NUMERIC_CHARACTERS' in env_vars.keys():
    #    nr_chars = env_vars['NLS_NUMERIC_CHARACTERS']
    #decimal_char = nr_chars[0]
    #value = value.replace(decimal_char, '.')
    #value = value.replace(' ', '')
    val = description.get('value', '')
    if description['sqltype'].find('NUMBER')!=-1:
        if str( description.get( 'value', '' ) ).find(',')!=-1:  # ASSUME THAT NLS_NUMERIC_CHARACTERS IS SET TO '. ' (DOT IN THE FIRST PLACE)
            val = str( val )
            val = val.replace(',', '.')
            try:
                return float(val)
            except Exception:
                pass
    elif description['sqltype'].find('DATE')!=-1:
        if type(val)==DateTime.DateTime.now():
            logger.info('datetime: %s' % (val.strftime('%Y-%m-%d')))
            return val.strftime('%Y-%m-%d')        
    #if description['sqltype'].find('DATE')!=-1:
    #    val = str(description.get('value', ''))
    #    #if isinstance(val, type('')):        
    #    return 'to_date(\'%s\',\'YYYY-MM-DD\')' % (val)
    return val
    

def get_ora_out_conv(description):
    """ Returns conversion function for type (output from database)
        @description - dictionary with: 'name', 'type', 'sqltype', 'precision', 'scale'        
    """
    sqltype = description['sqltype']
    if sqltype.find('VARCHAR')!=-1:
        return to_string
    elif sqltype.find('CHAR')!=-1:
        return to_string
    elif sqltype.find('LONG')!=-1:
        return to_string
    elif sqltype.find('NUMBER')!=-1 or sqltype.find('NUMERIC')!=-1 or sqltype.find('FLOAT')!=-1:
        if description.get('scale', 0)>0:
            return to_float
        elif str(description.get('value', '')).find('.')!=-1:  # ASSUME THAT NLS_NUMERIC_CHARACTERS IS SET TO '. ' (DOT IN THE FIRST PLACE)
            return to_float
        return to_int
    elif sqltype.find('INT')!=-1 or sqltype.find('DEC')!=-1:
        return to_int
    elif sqltype.find('DATE')!=-1:
        return to_datetime
    else:
        typy_f = open('nieznane_typy_danych_SQLRelaySP.txt', 'a+')
        typy_f.write('%s\n\r' % description['sqltype'])
        typy_f.close()
        return to_string

def to_string(value):
    """ Conversion to string
    """
    if value:
        return str(value)
    return value
    
def to_int(value):
    """ Conversion to int
    """
    if value:
        try:
            return int(value)
        except:
            return None
    return value
    
def to_float(value):
    """ Conversion to float
    """    
    if value:
        try:
            return float(value)
        except:
            return None
    return value
    
def to_datetime(value):
    """ Conversion to DateTime
    """
    from DateTime import DateTime
    
    if value:
        # assume date format as string: '31-12-9999 00:00:00'
        try:            
                data, czas = value.split(' ')
                dzien, miesiac, rok = data.split('-')
                godzina, minuta, sekunda = czas.split(':')            
                
                return DateTime(int(rok), int(miesiac), int(dzien), int(godzina), int(minuta), float(sekunda))
        except ValueError:
                try:
                        # assume date format as string '31-12-9999'
                        dzien, miesiac, rok = value.split('-')
                        return DateTime(int(rok), int(miesiac), int(dzien))
                except ValueError:
                        # assume date format as string '31/12/9999'
                        dzien, miesiac, rok = value.split('/')
                        return DateTime(int(rok), int(miesiac), int(dzien))
                
    return value
