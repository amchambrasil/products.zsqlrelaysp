"""
"""

# $Id: $

import string
from Shared.DC.ZRDB.Aqueduct import nicify
import Shared.DC.ZRDB.DA
from Shared.DC.ZRDB.DA import SQLMethodTracebackSupplement
from Products.ZSQLMethods.SQL import SQLConnectionIDs
import Products.ZSQLMethods.SQL
from Globals import HTMLFile
import Globals

try:
    from Globals import default__class_init__
except ImportError:
    from App.class_init import default__class_init__

from Globals import DTMLFile
from Shared.DC.ZRDB.Aqueduct import parse
import DocumentTemplate
from BTrees.IOBTree import IOBucket
import re
import sys
from time import time
from AccessControl import getSecurityManager
import sys, Globals, OFS.SimpleItem, AccessControl.Role
from DocumentTemplate.html_quote import html_quote
from string import atoi, find, join, split, rstrip
from coig_utils import get_ora_out_conv, get_ora_in_conv
from SQLRelay import PySQLRDB

from cgi import escape
from ExtensionClass import Base
from AccessControl.Owned import UnownableOwner

manage_addZSQLRelaySPForm = HTMLFile('dtml/addZSQLRelaySP', globals())
manage_addZSQLRelaySPForm.__name__ = 'addZSQLRelaySP'


def manage_addZSQLRelaySP(self, id, title,
                          connection_id, arguments,
                          template, in_parameters,
                          out_parameters, result_set,
                          REQUEST=None, submit=None):
    """Add an SQL Method

    @type  connection_id: string
    @param connection_id: the id of a database connection
        that resides in the current folder or in a folder above the
        current folder.  The database should understand SQL.

    @type  arguments: string
    @param arguments: arguments specifications, as would be given
        in the SQL method cration form.

    @type  template: string
    @param template: source for the SQL template.

    @type  in_parameters: string
    @param in_parameters: list of input parameters for procedure

    @type  out_parameters: string
    @param out_parameters: list of output parameters for procedure

    @type  result_set: string
    @param result_set: name of result_set parameter
    """

    self._setObject(id, ZSQLRelaySP(id, title,
                                    connection_id, arguments, template,
                                    in_parameters, out_parameters, result_set))
    if REQUEST is not None:
        u = REQUEST['URL1']
        if submit == "Add and Edit":
            u = "%s/%s/manage_main" % (u, id)
        elif submit == "Add and Test":
            u = "%s/%s/manage_testForm" % (u, id)
        else:
            u = u + '/manage_main'
        REQUEST.RESPONSE.redirect(u)
    return ''


class PM(Base):
    _owner = UnownableOwner

    _View_Permission = '_View_Permission'
    _is_wrapperish = 1

    def __getattr__(self, name):
        # We want to make sure that any non-explicitly set methods are
        # private!
        if name.startswith('_') and name.endswith("_Permission"):
            return ''
        raise AttributeError, escape(name)

PermissionMapper = PM


class ZSQLRelaySP(Shared.DC.ZRDB.DA.DA):
    """ ZSQLRelaySP
    """
    meta_type = 'ZSQLRelaySP'
    Database_Error = PySQLRDB.DatabaseError

    manage_options = (
        ({'label': 'Edit', 'action': 'manage_main',
          'help': ('ZSQLRelaySP', 'help/ZSQLRelaySP_Info.stx')},
         {'label': 'Test', 'action': 'manage_testForm',
          'help': ('ZSQLRelaySP', 'help/ZSQLRelaySP_Info.stx')},
         {'label': 'Advanced', 'action': 'manage_advancedForm',
          'help': ('ZSQLRelaySP', 'Z-SQL-Method_Advanced.stx')},
         )
        + AccessControl.Role.RoleManager.manage_options
        + OFS.SimpleItem.Item.manage_options
    )

    manage = manage_main = DTMLFile('dtml/editZSQLRelaySP', globals())
    manage_main._setName('manage_main')

    def __init__(self, id, title,
                 connection_id, arguments, template,
                 in_parameters, out_parameters, result_set):
        """ Initialization
        """
        self.id = str(id)
        self.manage_edit(title, connection_id,
                         arguments, template, in_parameters,
                         out_parameters, result_set)

    def _isBeingAccessedAsZClassDefinedInstanceMethod(self):
        p = getattr(self, 'aq_parent', None)
        #Not wrapped
        if p is None:
            return 0
        base = getattr(p, 'aq_base', None)
        return type(base) is PermissionMapper

    def _er(self, title, connection_id,
            arguments, template, in_parameters,
            out_parameters, result_set,
            SUBMIT, dtpref_cols, dtpref_rows, REQUEST):
        dr, dc = self._size_changes[SUBMIT]
        rows = str(max(1, int(dtpref_rows) + dr))
        cols = str(dtpref_cols)
        if cols.endswith('%'):
           cols = str(min(100, max(25, int(cols[:-1]) + dc))) + '%'
        else:
           cols = str(max(35, int(cols) + dc))
        e = (DateTime("GMT") + 365).rfc822()
        setCookie = REQUEST["RESPONSE"].setCookie
        setCookie("dtpref_rows", rows, path='/', expires=e)
        setCookie("dtpref_cols", cols, path='/', expires=e)
        REQUEST.other.update({"dtpref_cols": cols, "dtpref_rows": rows})
        return self.manage_main(self, REQUEST, title=title,
                                arguments_src=arguments,
                                connection_id=connection_id,
                                src=template, in_parameters=in_parameters,
                                out_parameters=out_parameters,
                                result_set=result_set)

    def manage_edit(self, title, connection_id,
                    arguments, template, in_parameters,
                    out_parameters, result_set,
                    SUBMIT='Change', dtpref_cols='50', dtpref_rows='20',
                    REQUEST=None):
        """ change data
        """

        if self._size_changes.has_key(SUBMIT):
            return self._er(title, connection_id, arguments,
                            template, in_parameters, out_parameters,
                            result_set, SUBMIT, dtpref_cols,
                            dtpref_rows, REQUEST)

        if self.wl_isLocked():
            raise ResourceLockedError, 'SQL Method is locked via WebDAV'

        self.title = str(title)
        self.connection_id = str(connection_id)
        arguments = str(arguments)
        self.arguments_src = arguments

        # ZSQLRelaySP params
        self.in_parameters = str(in_parameters).strip()
        self.in_params = self.get_params_descr(self.in_parameters, 'in')
        self.out_parameters = str(out_parameters).strip()
        self.out_params = self.get_params_descr(self.out_parameters, 'out')
        self.result_set = result_set.strip()

        self._arg = parse(arguments)

        template = str(template)
        self.src = template
        self.template=t=self.template_class(template)
        t.cook()
        self._v_cache = {}, IOBucket()
        if REQUEST:
            if SUBMIT == 'Change and Test':
                return self.manage_testForm(REQUEST)
            message = 'ZSQLRelaySP content changed'
            return self.manage_main(self, REQUEST, manage_tabs_message=message)
        return ''

    def get_params_descr(self, params, param_type='in'):
        """ Return dictionary with parameters description
            @ params - string: nazwa,typ,precision,scale (A_TYP_DOK_ID,NUMBER,14,5)
            @ params example - param1, 10, 2
                               param2
                               param3
                               param4,12,2
        """
        params_dict = {'params_list': []}

        for param in params.split('\n'):
            # jesli pusta linia to kontynuacja
            if param.strip() == '':
                continue

            # dzielenie po przecinku wiec dodajemy dwa przecinki aby zawsze otrzymac co najmniej 4 czesci
            param = '%s,,,' % (param)
            param_parts = param.split(',')

            # utworzenie slownika z opisem parametru
            descr = {}
            descr['name'] = param_parts[0].strip()
            try:
                # data_type
                descr['datatype'] = param_parts[1].strip().upper() or ''
                # precision
                descr['precision'] = int(param_parts[2].strip() or '0')
                descr['scale'] = int(param_parts[3].strip() or '0')  # scale
            except Exception, err:
                raise Exception, 'Precision, length and scale must be integers!: Error: \'%s\'' % (err)

            params_dict[descr['name']] = descr
            params_dict['params_list'].append(descr['name'])
        return params_dict

    def aa(self):
        from SQLRelay import PySQLRDB, PySQLRClient
        con = PySQLRClient.sqlrconnection('localhost', 9000, '', 'test', 'test', 0, 1)
        cur = PySQLRClient.sqlrcursor(con)
        cur.prepareQuery("begin  :curs:=testproc; end;")
        cur.defineOutputBindCursor("curs")
        cur.executeQuery()
        bindcur = cur.getOutputBindCursor("curs")
        bindcur.fetchFromBindCursor()
        con.endSession()

    def __call__(self, *args, **kw):
        """Call the stored procedure

        The arguments to the method should be passed via keyword
        arguments, or in a single mapping object. If no arguments are
        given, and if the method was invoked through the Web, then the
        method will try to acquire and use the Web REQUEST object as
        the argument mapping.

        The returned value is a sequence of record objects.
        """

        REQUEST = kw.get('REQUEST', None)
        __ick__ = kw.get('__ick__', None)
        src__ = kw.get('src__', 0)
        test__ = kw.get('test__', 0)

        __traceback_supplement__ = (SQLMethodTracebackSupplement, self)

        if REQUEST is None:
            if kw:
                EQUEST = kw
            else:
                if hasattr(self, 'REQUEST'):
                    REQUEST = self.REQUEST
                else:
                    REQUEST = {}

        # connection hook
        c = self.connection_id
        # for backwards compatability
        hk = self.connection_hook
        # go get the connection hook and call it
        if hk:
            c = getattr(self, hk)()
        try:
            dbc = getattr(self, c)
        except AttributeError:
            raise AttributeError, (
                "The database connection <em>%s</em> cannot be found." % (
                c))

        try:
            DB__ = dbc()
        except:
            raise self.Database_Error, ( '%s is not connected to a database' % self.id )

        if hasattr(self, 'aq_parent'):
            p = self.aq_parent
            if self._isBeingAccessedAsZClassDefinedInstanceMethod():
                p = p.aq_parent
        else:
            p = None

        argdata = self._argdata(REQUEST)
        argdata['sql_delimiter'] = '\0'
        argdata['sql_quote__'] = dbc.sql_quote__

        security = getSecurityManager()
        security.addContext(self)
        try:
            try:
                query = apply(self.template, (p,), argdata)
            except TypeError, msg:
                msg = str(msg)
                if find(msg, 'client') >= 0:
                    raise NameError("'client' may not be used as an " +
                                    "argument name in this context")
                else:
                    raise
        finally:
            security.removeContext(self)

        if src__:
            return query
        DB__._register()
        #con=PySQLRClient.sqlrconnection('host',9000,'','user','password',0,1)
        from SQLRelay import PySQLRClient
        cur = PySQLRClient.sqlrcursor(DB__.con)
        cur.getNullsAsNone()
        #cur = DB__.cur

        # zwracane wartosci
        results = []
        cur.clearBinds()
        cur.prepareQuery(query)

        # przypisanie wszystkich parametrow do kw
        argc = 0
        argl = len(args)
        _notfound = []

        for param_key in self.in_params['params_list']:
            argc = argc + 1
            if len(args) >= argc and not kw.has_key( param_key ):
                kw[ param_key ] = args[argc-1]
            else:
                v = REQUEST.get(param_key, None)
                if (not kw.has_key( param_key )):
                    kw[param_key] = v
        # IN
        for param_key in self.in_params['params_list']:
            descr = self.in_params[param_key]
            descr_dict = {'sqltype':descr['datatype'], 'value':kw[param_key]}

            if descr['datatype'].find('CLOB')!=-1:
                cur.inputBindClob( param_key, kw[param_key], len(kw[param_key]) )
            elif descr['datatype'].find('BLOB')!=-1:
                cur.inputBindBlob( param_key, kw[param_key], len(kw[param_key]) )
            else:
                val = get_ora_in_conv(descr_dict)
                precision=0
                scale=0
                if type(val) == type(1.1):
                    precision = len(str(val))-1
                    scale = precision-len(str(int(val)))-1
                state = cur.inputBind(param_key, val, precision, scale)
                if not state:
                    raise self.Database_Error('SQLRelaySP: Unable to bind: %s' % (param_key))

        # OUT
        for param_key in self.out_params['params_list']:
            descr = self.out_params[param_key]
            datatype = descr['datatype']
            precision = descr['precision']
            scale = descr['scale']

            #number, dec, decimal, double, float, real, numeric
            if datatype.find('CLOB')!=-1:
                cur.defineOutputBindClob(param_key)
            elif datatype.find('BLOB')!=-1:
                cur.defineOutputBindBlob(param_key)
            elif datatype.find('NUMBER')!=-1 or \
                  datatype.find('NUMERIC')!=-1 or \
                  datatype.find('DEC')!=-1 or \
                  datatype.find('DOUBLE')!=-1 or \
                  datatype.find('REAL')!=-1 or \
                  datatype.find('FLOAT')!=-1:
                if scale==0:
                    cur.defineOutputBindInteger(param_key)
                else:
                    cur.defineOutputBindDouble(param_key)
            elif datatype.find('INT')!=-1 or datatype.find('ROWID')!=-1:
                #integer, int, smallint, rowid
                cur.defineOutputBindInteger(param_key)
            else:
                #varchar2, long, raw, long raw, date, timestamp, nchar, nvarchar
                cur.defineOutputBindString(param_key, descr['precision'])

        # CURSOR
        if self.result_set != '':
            cur.defineOutputBindCursor(self.result_set)

        res = cur.executeQuery()
        failures = 0
        last_call_time = time()
        try:
            if not res:
                raise self.Database_Error( cur.errorMessage() )
        except self.Database_Error, mess:
            failures = failures+1
            if (failures > 1000 or time()-last_call_time > 600):
                # Hm. maybe the db is hosed.  Let's try again.
                failures = 0
                last_call_time = time()
                return self(REQUEST=REQUEST, __ick__=__ick__, src__=src__, test__=test__, **kw)
            else:
                raise sys.exc_type, sys.exc_value, sys.exc_traceback

        # FUNCTION RETURN VALUE
        #if self.function!='':
        #    value = cur.getField(0,0)
        #    descr_dict = {'sqltype':self.function, 'value':value}
        #    conv = get_ora_conversion(descr_dict)
        #    #results['out_res'][param_key] = conv(value)
        #    results.append(conv(value))

        # OUT BINDINGS
        if len(self.out_params['params_list']) > 0:
            for param_key in self.out_params['params_list']:
                descr = self.out_params[param_key]
                datatype = descr['datatype']
                precision = descr['precision']
                scale = descr['scale']

                #number, dec, decimal, double, float, real, numeric
                if datatype.find('CLOB') != -1:
                    results.append(cur.getOutputBindClob(param_key))
                    continue
                elif datatype.find('BLOB') != -1:
                    results.append(cur.getOutputBindBlob(param_key))
                    continue
                elif datatype.find('NUMBER') != -1 or \
                      datatype.find('NUMERIC')!=-1 or \
                      datatype.find('DEC')!=-1 or \
                      datatype.find('DOUBLE')!=-1 or \
                      datatype.find('REAL')!=-1 or \
                      datatype.find('FLOAT')!=-1:
                    if scale==0:
                        value = cur.getOutputBindInteger(param_key)
                    else:
                        value = cur.getOutputBindDouble(param_key)
                elif datatype.find('INT')!=-1 or datatype.find('ROWID')!=-1:
                    #integer, int, smallint, rowid
                    value = cur.getOutputBindInteger(param_key)
                else:
                    #varchar2, long, raw, long raw, date, timestamp, nchar, nvarchar
                    value = cur.getOutputBindString(param_key)

                descr_dict = {'sqltype':descr['datatype'], 'value':value}
                conv = get_ora_out_conv(descr_dict)
                #results['out_res'][param_key] = conv(value)
                results.append(conv(value))

        # CURSOR
        if self.result_set!='':
            bindcur=cur.getOutputBindCursor(self.result_set.strip())
            bindcur.fetchFromBindCursor()
            results.extend(bindcur.getRowRange(0, bindcur.rowCount()))

        if len(results)==1:
            results = results[0]

        if test__: return query, results
        return results

        if self.cache_time_ > 0 and self.max_cache_ > 0:
            result=self._cached_result(DB__, (query, self.max_rows_))
        else:
            result = DB__.query(query, self.max_rows_)

        if hasattr(self, '_v_brain'): brain=self._v_brain
        else:
            brain=self._v_brain=getBrain(self.class_file_, self.class_name_)

        zc=self._zclass
        if zc is not None: zc=zc._zclass_

        if type(result) is type(''):
            f=StringIO()
            f.write(result)
            f.seek(0)
            result=RDB.File(f,brain,p, zc)
        else:
            result=Results(result, brain, p, zc)
        columns=result._searchable_result_columns()
        if test__ and columns != self._col: self._col=columns

        # If run in test mode, return both the query and results so
        # that the template doesn't have to be rendered twice!
        if test__: return query, result

        return result

    def manage_testForm(self, REQUEST):
        " "
        input_src=self.default_input_form(self.title_or_id(),
                                     self._arg, self.in_parameters, self.result_set, 'manage_test',
                                     '<dtml-var manage_tabs>')
        return DocumentTemplate.HTML(input_src)(self, REQUEST, HTTP_REFERER='')

    def manage_test(self, REQUEST):
        """Test an SQLRelaySQLSP."""
        # Try to render the query template first so that the rendered
        # source will be available for the error message in case some
        # error occurs...
        try:
            src = self(REQUEST, src__=1)
        except:
            src = "Could not render the query template!"
        result = ()
        t=v=tb=None
        try:
            try:
                src, result=self(REQUEST, test__=1)
                if find(src,'\0'):
                    src=join(split(src,'\0'),'\n'+'-'*60+'\n')

                r = None
                try:
                    if len(result)>0:
                        r=str(result)
                except:
                    if result:
                        r = result
                if not r:
                    r='This statement returned no results.'
            except:
                t, v, tb = sys.exc_info()
                r='<strong>Error, <em>%s</em>:</strong> %s' % (t, v)

            report=DocumentTemplate.HTML(
                '<html>\n'
                '<BODY BGCOLOR="#FFFFFF" LINK="#000099" VLINK="#555555">\n'
                '<dtml-var manage_tabs>\n<hr>\n%s\n\n'
                '<hr><strong>SQL used:</strong><br>\n<pre>\n%s\n</pre>\n<hr>\n'
                '</body></html>'
                % (r, html_quote(src)))

            report=apply(report,(self,REQUEST),{self.id:result})

            if tb is not None:
                self.raise_standardErrorMessage(
                    None, REQUEST, t, v, tb, None, report)

            return report

        finally:
            tb=None

    def default_input_form(self, id, arguments, in_parameters, result_set, action='query',
                           tabs=''):
        if not (arguments or in_parameters or result_set):
            return (
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">\n'
                '<html lang="en"><head><title>%s Input Data</title></head>\n'
                '<body bgcolor="#FFFFFF" link="#000099" vlink="#555555">\n%s\n'
                '<form action="&dtml-URL2;/&dtml-id;/%s" '
                'method="get">\n'
                '<h2>%s Input Data</h2>\n'
                'This query requires no input.<p>\n'
                '<input type="SUBMIT" name="SUBMIT" value="Submit Query">\n'
                '<dtml-if HTTP_REFERER>\n'
                '  <input type="SUBMIT" name="SUBMIT" value="Cancel">\n'
                '  <INPUT NAME="CANCEL_ACTION" TYPE="HIDDEN"\n'
                '         VALUE="&dtml-HTTP_REFERER;">\n'
                '</dtml-if>\n'
                '</td></tr>\n</table>\n</form>\n</body>\n</html>\n'
                % (id, tabs, action, id)
                )

        out = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">\n
                    <html lang="en"><head><title>%s Input Data</title></head>\n
                    <body bgcolor="#FFFFFF" link="#000099" vlink="#555555">\n%s\n
                    <form action="&dtml-URL2;/&dtml-id;/%s"
                    method="get">\n
                    <h2>%s Input Data</h2>\n
                    Enter query parameters:<br>
                    <table>\n""" % (id, tabs, action,id)
        if arguments:
            items=arguments.items()
            out += "\n%s" % (string.joinfields(map(lambda a:
                                                    ('<tr> <th>%s</th>\n'
                                                     '     <td><input name="%s"\n'
                                                     '                size="30" value="%s">'
                                                     '     </td></tr>'
                                                     % (nicify(a[0]),
                                                        (
                                                            a[1].has_key('type') and
                                                            ("%s:%s" % (a[0],a[1]['type'])) or
                                                            a[0]
                                                                ),
                                                        a[1].has_key('default') and a[1]['default'] or ''
                                                        ))
                                                    , items
                                                    ),
                                                '\n') )

        input = '''<tr><th>%s</th>\n
                       <td>
                           <input name="%s" size="30" value="" />
                       </td></tr>'''

        if len(self.in_params[ 'params_list' ])>0:
            out += '''<tr><th colspan="2">In parameters</th></tr>'''
        for param_key in self.in_params[ 'params_list' ]:
            out += input % (param_key, param_key)

        #if result_set:
        #    out += '''<tr><th colspan="2">Result set</th></tr>'''
        #    out += input % (result_set, result_set)

        out += """\n<tr><td colspan=2 align=center>\n
                        <input type="SUBMIT" name="SUBMIT" value="Submit Query">\n
                        <dtml-if HTTP_REFERER>\n
                          <input type="SUBMIT" name="SUBMIT" value="Cancel">\n
                          <INPUT NAME="CANCEL_ACTION" TYPE="HIDDEN"\n
                                 VALUE="&dtml-HTTP_REFERER;">\n
                        </dtml-if>\n
                        </td></tr>\n</table>\n</form>\n</body>\n</html>\n"""

        return (out)

    def custom_default_report(id, result, action='', no_table=0,
                              goofy=re.compile('\W').search):
        columns=result._searchable_result_columns()
        __traceback_info__=columns
        heading=('<tr>\n%s        </tr>' %
                     string.joinfields(
                         map(lambda c:
                             '          <th>%s</th>\n' % nicify(c['name']),
                             columns),
                         ''
                         )
                     )

        if no_table: tr, _tr, td, _td, delim = '<p>', '</p>', '', '', ',\n'
        else: tr, _tr, td, _td, delim = '<tr>', '</tr>', '<td>', '</td>', '\n'

        row=[]
        for c in columns:
            n=c['name']
            if goofy(n) is not None:
                n='expr="_[\'%s]"' % (`'"'+n`[2:])
            row.append('          %s<dtml-var %s%s>%s'
                       % (td,n,c['type']!='s' and ' null=""' or '',_td))

        row=('     %s\n%s\n        %s' % (tr,string.joinfields(row,delim), _tr))

        return custom_default_report_src(
            id=id,heading=heading, row=row, action=action, no_table=no_table)

    def custom_default_zpt_report(id, result, action='', no_table=0,
                              goofy=re.compile('\W').search
                              ):
        columns=result._searchable_result_columns()
        __traceback_info__=columns
        heading=('<tr>\n%s        </tr>' %
                    string.joinfields(
                         map(lambda c:
                             '          <th>%s</th>\n' % nicify(c['name']),
                             columns),
                         ''
                         )
                     )

        if no_table: tr, _tr, td, _td, delim = '<p>', '</p>', '', '', ',\n'
        else: tr, _tr, td, _td, delim = '<tr>', '</tr>', '<td>', '</td>', '\n'

        row=[]
        for c in columns:
            n=c['name']
        # ugh! what the hell is goofy?
        #        if goofy(n) is not None:
        #            n='expr="_[\'%s]"' % (`'"'+n`[2:])
            row.append('          %s<span tal:replace="result/%s">%s goes here</span>%s'
                       % (td,n,n,_td))

        row = ('     %s\n%s\n        %s' % (tr,string.joinfields(row,delim), _tr))

        return custom_default_zpt_report_src(
            id=id, heading=heading, row=row, action=action, no_table=no_table)

default__class_init__(ZSQLRelaySP)


def _getPhysicalPath(object):
    o = object
    gp = getattr(o, 'getPhysicalPath', None)
    if gp is not None:
        return gp()
    from sys import stdout
    l = []
    while o:
        try:
            id = o.id
        except AttributeError:
            break
        if callable(id):
            id = id()
        l.append(id)
        o = getattr(o, 'aq_inner', None)
        if o is not None:
            o = o.aq_parent
    l.reverse()
    return tuple(l)
