"""ZSQLRelaySP Product"""

# $Id: $
from Products.ZSQLRelaySP import ZSQLRelaySP
import Shared.DC.ZRDB.Search


def initialize(context):

    context.registerClass(
        ZSQLRelaySP.ZSQLRelaySP,
        permission='Add Database Methods',
        constructors=(ZSQLRelaySP.manage_addZSQLRelaySPForm, ZSQLRelaySP.manage_addZSQLRelaySP),
        icon='ZSQLRelaySP.gif'
    )

    context.registerHelp()
    context.registerHelpTitle('ZSQLRelaySP Help')

methods = {
    'SQLConnectionIDs': ZSQLRelaySP.SQLConnectionIDs,
    'ZQueryIds': Shared.DC.ZRDB.Search.ZQueryIds,
}

__ac_permissions__ = (('Open/Close Database Connections', ()), )
